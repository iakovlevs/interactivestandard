package api;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;

public class GetUserListTest {
    private final static String URL = "https://hr-challenge.interactivestandard.com/api/test/users";

    @Test
    public void makeValidRequestWithGenderMale() {

        given()
                .expect()
                .body("success", equalTo(true))
                .body("errorCode", equalTo(0))
                .body("result", is(not(equalTo(null))))
                .when()
                .get(URL + "?gender=male")
                .then().log().all()
                .statusCode(200);
    }

    @Test
    public void makeValidRequestWithGenderFemale() {

        given()
                .expect()
                .body("success", equalTo(true))
                .body("errorCode", equalTo(0))
                .body("result", is(not(equalTo(null))))
                .when()
                .get(URL + "?gender=female")
                .then().log().all()
                .statusCode(200);
    }

    @Test
    public void makeValidRequestWithInvalidGender() {
        String userGender = "fmale";

        given()
                .expect()
                .body("error", equalTo("Internal Server Error"))
                .body("message", equalTo("No enum constant com.coolrocket.app.api.test.qa.Gender." + userGender))
                .when()
                .get(URL + "?gender=" + userGender)
                .then().log().all()
                .statusCode(500);
    }

    @Test
    public void makeValidRequestWithoutGender() {

        given()
                .expect()
                .body("error", equalTo("Internal Server Error"))
                .body("message", equalTo("No enum constant com.coolrocket.app.api.test.qa.Gender."))
                .when()
                .get(URL + "?gender=")
                .then().log().all()
                .statusCode(500);
    }

    @Test
    public void makeValidRequestWithRussianNameGender() {
        String userGender = "мужской";

        given()
                .expect()
                .body("error", equalTo("Internal Server Error"))
                .body("message", equalTo("No enum constant com.coolrocket.app.api.test.qa.Gender."+ userGender))
                .when()
                .get(URL + "?gender=" + userGender)
                .then().log().all()
                .statusCode(500);
    }

    @Test
    public void makeValidRequestWithoutQueryGender() {

        given()
                .expect()
                .body("error", equalTo("Bad Request"))
                .body("message", equalTo("Required String parameter 'gender' is not present"))
                .when()
                .get(URL)
                .then().log().all()
                .statusCode(400);
    }
}
