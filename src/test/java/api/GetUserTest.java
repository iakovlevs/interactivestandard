package api;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class GetUserTest {
    private final static String URL = "https://hr-challenge.interactivestandard.com/api/test/user/";

    @Test
    public void makeValidRequest() {
        given()
                .expect()
                .body("success", equalTo(true))
                .body("errorCode", equalTo(0))
                .body("result.id", equalTo("10"))
                .body("result.name", equalTo("Peter"))
                .body("result.gender", equalTo("Male"))
                .body("result.age", equalTo("26"))
                .body("result.city", equalTo("Omsk"))
                .body("result.registrationDate", equalTo("2016-12-01T00:30:00"))
                .when()
                .get(URL + "10")
                .then().log().all()
                .statusCode(200);
    }

    @Test
    public void makeRequestWithNonExistUserId() {
        given()
                .expect()
                .body("error", equalTo("Internal Server Error"))
                .body("message", equalTo("No message available"))
                .when()
                .get(URL + "0")
                .then().log().all()
                .statusCode(500);
    }

    @Test
    public void makeRequestWithDoubleId() {
        double userId = 2.5;

        given()
                .expect()
                .body("errorMessage", equalTo("NumberFormatException: For input string: \""+ userId + "\""))
                .body("user", equalTo(null))
                .when()
                .get(URL + userId)
                .then().log().all()
                .statusCode(400);
    }

    @Test
    public void makeRequestWithString() {
        String userId = "userid";

        given()
                .expect()
                .body("errorMessage", equalTo("NumberFormatException: For input string: \""+ userId + "\""))
                .body("user", equalTo(null))
                .when()
                .get(URL + userId)
                .then().log().all()
                .statusCode(400);
    }

    @Test
    public void makeRequestWithoutUserId() {

        given()
                .expect()
                .body("error", equalTo("Not Found"))
                .body("message", equalTo("No message available"))
                .when()
                .get(URL)
                .then().log().all()
                .statusCode(404);
    }

    @Test
    public void makeRequestWithNegativeUserId() {

        given()
                .expect()
                .body("error", equalTo(null))
                .body("message", equalTo(null))
                .when()
                .get(URL + "-1")
                .then().log().all()
                .statusCode(200);
    }
}